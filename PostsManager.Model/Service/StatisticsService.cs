﻿using System;
using System.Collections.Generic;
using System.Linq;
using PostsManager.Model.Service.Interface;

namespace PostsManager.Model.Service
{
	/// <summary>
	/// Сервис статистики.
	/// </summary>
	public class StatisticsService : IStatisticsService
	{
		/// <summary>
		/// Возвращает частотность букв в тексте.
		/// Если текст принимает значение null, то вернет пустой словарь.
		/// </summary>
		/// <param name="text">Текст.</param>
		/// <returns>Словарь соответствия буквы её частоте.</returns>
		public Dictionary<char, double> GetLetterFrequency(string text)
		{
			text = text ?? string.Empty;

			// Получаем все буквы в нижнем регистре.
			string text_letters = new string(text.Where(char.IsLetter).ToArray())
				.ToLower();

			return text_letters.ToList()
				// Группируем для подсчета количества.
				.GroupBy(c => c, (c, list) => new Tuple<char, double>(c, list.Count()))
				// Получаем частоты.
				.ToDictionary(p => p.Item1, p => p.Item2 / text_letters.Length);
		}
	}
}