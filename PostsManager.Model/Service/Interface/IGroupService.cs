﻿using PostsManager.Model.DataModel;

namespace PostsManager.Model.Service.Interface
{
	/// <summary>
	/// Интерфейс сервиса групп.
	/// </summary>
	public interface IGroupService
	{
		/// <summary>
		/// Возвращает группу по короткому имени.
		/// Если группа не найдена, вернет null.
		/// </summary>
		/// <param name="screen_name">Короткое имя.</param>
		/// <returns>Группа.</returns>
		Group GetByScreenName(string screen_name);
	}
}