﻿using System.Collections.Generic;
using PostsManager.Model.DataModel;

namespace PostsManager.Model.Service.Interface
{
	/// <summary>
	/// Интерфейс сервиса постов.
	/// </summary>
	public interface IPostService
	{
		/// <summary>
		/// Возвращает посты со страницы пользователя.
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <param name="count">Ограничение по количеству постов.</param>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные пользователем.</param>
		/// <returns>Перечисление постов.</returns>
		IEnumerable<Post> GetByUserId(long user_id, int count, bool only_from_owner = false);

		/// <summary>
		/// Возвращает посты со страницы группы.
		/// </summary>
		/// <param name="group_id">Идентификатор группы.</param>
		/// <param name="count">Ограничение по количеству постов.</param>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные группой.</param>
		/// <returns>Перечисление постов.</returns>
		IEnumerable<Post> GetByGroupId(long group_id, int count, bool only_from_owner = false);

		/// <summary>
		/// Публикует пост.
		/// </summary>
		/// <param name="post">Пост.</param>
		void Publish(Post post);
	}
}