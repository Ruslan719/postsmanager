﻿using PostsManager.Model.DataModel;

namespace PostsManager.Model.Service.Interface
{
	/// <summary>
	/// Интерфейс сервиса пользователей.
	/// </summary>
	public interface IUserService
	{
		/// <summary>
		/// Возвращает пользователя по короткому имени.
		/// Если пользователь не найден, вернет null.
		/// </summary>
		/// <param name="screen_name">Короткое имя.</param>
		/// <returns>Пользователь.</returns>
		User GetByScreenName(string screen_name);
	}
}