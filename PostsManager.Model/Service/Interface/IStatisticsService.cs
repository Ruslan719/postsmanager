﻿using System.Collections.Generic;

namespace PostsManager.Model.Service.Interface
{
	/// <summary>
	/// Интерфейс сервиса статистики.
	/// </summary>
	public interface IStatisticsService
	{
		/// <summary>
		/// Возвращает частотность букв в тексте.
		/// </summary>
		/// <param name="text">Текст.</param>
		/// <returns>Словарь соответствия буквы её частоте.</returns>
		Dictionary<char, double> GetLetterFrequency(string text);
	}
}