﻿namespace PostsManager.Model.Service.Interface
{
	/// <summary>
	/// Интерфейс сервиса авторизации.
	/// </summary>
	public interface IAuthService
	{
		/// <summary>
		/// Авторизует текущего пользователя и возвращает его идентификатор.
		/// </summary>
		/// <param name="login">Логин.</param>
		/// <param name="password">Пароль.</param>
		/// <returns>Идентификатор пользователя.</returns>
		long Authorize(string login, string password);

		/// <summary>
		/// Проверяет, авторизован ли текущий пользователь.
		/// </summary>
		/// <returns>true, если авторизован, иначе - false.</returns>
		bool IsAuthorized();
	}
}