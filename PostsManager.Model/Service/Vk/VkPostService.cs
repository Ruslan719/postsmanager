﻿using System;
using System.Collections.Generic;
using System.Linq;
using PostsManager.Model.DataModel;
using PostsManager.Model.Exception;
using PostsManager.Model.Service.Interface;
using VkNet.Abstractions;
using VkNet.Enums.SafetyEnums;
using VkNet.Exception;
using VkNet.Model.RequestParams;

namespace PostsManager.Model.Service.Vk
{
	/// <summary>
	/// Сервис постов ВКонтакте.
	/// </summary>
	public class VkPostService : IPostService
	{
		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private readonly IVkApi _vkApi;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="vk_api">API для работы с ВКонтакте.</param>
		public VkPostService(IVkApi vk_api)
		{
			_vkApi = vk_api;
		}

		/// <summary>
		/// Возвращает посты со страницы пользователя.
		/// </summary>
		/// <param name="user_id">Идентификатор пользователя.</param>
		/// <param name="count">Ограничение по количеству постов.</param>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные пользователем.</param>
		/// <returns>Перечисление постов.</returns>
		public IEnumerable<Post> GetByUserId(long user_id, int count, bool only_from_owner = false) =>
			GetByOwnerId(user_id, count, only_from_owner, OwnerType.User);

		/// <summary>
		/// Возвращает посты со страницы группы.
		/// </summary>
		/// <param name="group_id">Идентификатор группы.</param>
		/// <param name="count">Ограничение по количеству постов.</param>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные группой.</param>
		/// <returns>Перечисление постов.</returns>
		public IEnumerable<Post> GetByGroupId(long group_id, int count, bool only_from_owner = false) =>
			GetByOwnerId(group_id, count, only_from_owner, OwnerType.Group);

		/// <summary>
		/// Публикует пост.
		/// </summary>
		/// <param name="post">Пост.</param>
		public void Publish(Post post) =>
			_vkApi.Wall.Post(new WallPostParams
			{
				// В API группы и пользователи различаются знаком идентификатора.
				// В модели приложения для этого используется Enum, и все идентификаторы положительные.
				OwnerId = post.OwnerType == OwnerType.Group ? -Math.Abs(post.OwnerId) : Math.Abs(post.OwnerId),
				Message = post.Text
			});

		/// <summary>
		/// Возвращает посты со страницы владельца.
		/// </summary>
		/// <param name="owner_id">Идентификатор владельца</param>
		/// <param name="count">Ограничение по количеству постов.</param>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные владельцем.</param>
		/// <param name="owner_type">Тип владельца.</param>
		/// <returns>Перечисление постов.</returns>
		private IEnumerable<Post> GetByOwnerId(long owner_id, int count, bool only_from_owner, OwnerType owner_type)
		{
			try
			{
				return _vkApi.Wall.Get(new WallGetParams
					{
						OwnerId = owner_type == OwnerType.Group ? -Math.Abs(owner_id) : Math.Abs(owner_id),
						Count = (uint) count,
						Filter = only_from_owner ? WallFilter.Owner : WallFilter.All
					})
					.WallPosts.Select(p => new Post
					{
						Id = p.Id,
						OwnerId = Math.Abs(p.OwnerId.Value),
						OwnerType = owner_type,
						Text = p.Text
					});
			}
			catch (UserDeletedOrBannedException)
			{
				throw new PostsManagerException("Страница удалена или заблокирована, или ещё не создана.");
			}
		}
	}
}