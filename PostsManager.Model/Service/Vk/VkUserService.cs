﻿using System.Linq;
using PostsManager.Model.Service.Interface;
using VkNet.Abstractions;
using VkNet.Enums;
using VkNet.Model;
using User = PostsManager.Model.DataModel.User;

namespace PostsManager.Model.Service.Vk
{
	/// <summary>
	/// Сервис пользователей ВКонтакте.
	/// </summary>
	public class VkUserService : IUserService
	{
		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private readonly IVkApi _vkApi;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="vk_api">API для работы с ВКонтакте.</param>
		public VkUserService(IVkApi vk_api)
		{
			_vkApi = vk_api;
		}

		/// <summary>
		/// Возвращает пользователя по короткому имени.
		/// Если пользователь не найден, вернет null.
		/// </summary>
		/// <param name="screen_name">Короткое имя.</param>
		/// <returns>Пользователь.</returns>
		public User GetByScreenName(string screen_name)
		{
			VkObject vk_object = _vkApi.Utils.ResolveScreenName(screen_name);

			if (vk_object == null || vk_object.Type != VkObjectType.User || !vk_object.Id.HasValue)
			{
				return null;
			}

			return _vkApi.Users.Get(new[] {vk_object.Id.Value})
				.Select(u => new User
				{
					Id = u.Id,
					FullName = $"{u.FirstName} {u.LastName}"
				})
				.FirstOrDefault();
		}
	}
}