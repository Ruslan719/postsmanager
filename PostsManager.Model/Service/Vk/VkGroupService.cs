﻿using System.Linq;
using PostsManager.Model.Service.Interface;
using VkNet.Abstractions;
using VkNet.Enums;
using VkNet.Model;
using Group = PostsManager.Model.DataModel.Group;

namespace PostsManager.Model.Service.Vk
{
	/// <summary>
	/// Сервис групп ВКонтакте.
	/// </summary>
	public class VkGroupService : IGroupService
	{
		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private readonly IVkApi _vkApi;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="vk_api">API для работы с ВКонтакте.</param>
		public VkGroupService(IVkApi vk_api)
		{
			_vkApi = vk_api;
		}

		/// <summary>
		/// Возвращает группу по короткому имени.
		/// Если группа не найдена, вернет null.
		/// </summary>
		/// <param name="screen_name">Короткое имя.</param>
		/// <returns>Группа.</returns>
		public Group GetByScreenName(string screen_name)
		{
			VkObject vk_object = _vkApi.Utils.ResolveScreenName(screen_name);

			if (vk_object == null || vk_object.Type != VkObjectType.Group || !vk_object.Id.HasValue)
			{
				return null;
			}

			return _vkApi.Groups.GetById(null, screen_name, null)
				.Select(g => new Group
				{
					Id = g.Id,
					Name = g.Name
				})
				.FirstOrDefault();
		}
	}
}