﻿using Microsoft.Extensions.Configuration;
using PostsManager.Model.Exception;
using PostsManager.Model.Service.Interface;
using VkNet.Abstractions;
using VkNet.Enums.Filters;
using VkNet.Model;

namespace PostsManager.Model.Service.Vk
{
	/// <summary>
	/// Сервис авторизации ВКонтакте.
	/// </summary>
	public class VkAuthService : IAuthService
	{
		/// <summary>
		/// Название параметра конфигурации для идентификатора приложения ВКонтакте.
		/// </summary>
		public const string VK_APPLICATION_ID_SECTION = "VkApplicationId";

		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private readonly IVkApi _vkApi;

		/// <summary>
		/// Интерфейс работы с конфигурацией приложения.
		/// </summary>
		private readonly IConfiguration _configuration;

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="vk_api">API для работы с ВКонтакте.</param>
		/// <param name="configuration">Интерфейс работы с конфигурацией приложения.</param>
		public VkAuthService(IVkApi vk_api, IConfiguration configuration)
		{
			_vkApi = vk_api;
			_configuration = configuration;
		}

		/// <summary>
		/// Авторизует текущего пользователя и возвращает его идентификатор.
		/// </summary>
		/// <param name="login">Логин.</param>
		/// <param name="password">Пароль.</param>
		/// <returns>Идентификатор пользователя.</returns>
		public long Authorize(string login, string password)
		{
			if (!ulong.TryParse(_configuration[VK_APPLICATION_ID_SECTION], out ulong application_id))
			{
				throw new ConfigurationException(VK_APPLICATION_ID_SECTION);
			}
			
			_vkApi.Authorize(new ApiAuthParams
			{
				ApplicationId = application_id,
				Login = login,
				Password = password,
				Settings = Settings.Wall
			});

			return _vkApi.UserId.Value;
		}

		/// <summary>
		/// Проверяет, авторизован ли текущий пользователь.
		/// </summary>
		/// <returns>true, если авторизован, иначе - false.</returns>
		public bool IsAuthorized() => _vkApi.IsAuthorized;
	}
}