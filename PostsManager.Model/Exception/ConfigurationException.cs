﻿namespace PostsManager.Model.Exception
{
	/// <summary>
	/// Исключение, выбрасываемое при неудачном получении параметра конфигурации.
	/// </summary>
	public class ConfigurationException : System.Exception
	{
		/// <summary>
		/// Название параметра конфигурации.
		/// </summary>
		private readonly string _section;

		/// <summary>
		/// Сообщение исключения.
		/// </summary>
		public override string Message => $"Отсутствует или задан неверно параметр конфигурации с именем {_section}";

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="section">Название параметра конфигурации.</param>
		public ConfigurationException(string section)
		{
			_section = section;
		}
	}
}
