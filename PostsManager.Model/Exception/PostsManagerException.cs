﻿namespace PostsManager.Model.Exception
{
	/// <summary>
	/// Исключение, выбрасываемое при некорректном использовании модели.
	/// </summary>
	public class PostsManagerException : System.Exception
	{
		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="message">Сообщение исключения.</param>
		public PostsManagerException(string message) : base(message)
		{
		}
	}
}
