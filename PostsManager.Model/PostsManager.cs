﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PostsManager.Model.DataModel;
using PostsManager.Model.Exception;
using PostsManager.Model.Service.Interface;

namespace PostsManager.Model
{
	/// <summary>
	/// Основные методы для работы с моделью.
	/// </summary>
	public class PostsManager
	{
		#region Fields

		/// <summary>
		/// Название параметра конфигурации для ограничения количества обрабатываемых постов.
		/// </summary>
		public const string POSTS_COUNT_SECTION = "PostsCount";

		/// <summary>
		/// Сервис авторизации.
		/// </summary>
		private readonly IAuthService _authService;

		/// <summary>
		/// Сервис групп.
		/// </summary>
		private readonly IGroupService _groupService;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private readonly IUserService _userService;

		/// <summary>
		/// Сервис постов.
		/// </summary>
		private readonly IPostService _postService;

		/// <summary>
		/// Сервис статистики.
		/// </summary>
		private readonly IStatisticsService _statisticsService;

		/// <summary>
		/// Интерфейс работы с конфигурацией приложения.
		/// </summary>
		private readonly IConfiguration _configuration;

		/// <summary>
		/// Модуль журналирования.
		/// </summary>
		private readonly ILogger<PostsManager> _logger;

		#endregion

		#region Constructors

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="auth_service">Сервис авторизации.</param>
		/// <param name="group_service">Сервис групп.</param>
		/// <param name="user_service">Сервис пользователей.</param>
		/// <param name="post_service">Сервис постов.</param>
		/// <param name="statistics_service">Сервис статистики.</param>
		/// <param name="configuration">Интерфейс работы с конфигурацией приложения.</param>
		/// <param name="logger">Модуль журналирования.</param>
		public PostsManager(IAuthService auth_service, IGroupService group_service, IUserService user_service,
			IPostService post_service, IStatisticsService statistics_service, IConfiguration configuration,
			ILogger<PostsManager> logger)
		{
			_authService = auth_service;
			_groupService = group_service;
			_userService = user_service;
			_postService = post_service;
			_statisticsService = statistics_service;
			_configuration = configuration;
			_logger = logger;
		}

		#endregion

		#region Methods/Public

		/// <summary>
		/// Формирует информацию о частотности букв в постах страницы с указанным коротким именем и 
		/// публикует её на странице указанного пользователя.
		/// Перед выполнением необходима авторизация.
		/// </summary>
		/// <param name="screen_name">Короткое имя страницы с исследуемыми постами.</param>
		/// <param name="user_id">Идентификатор пользователя, на странице которого опубликуется пост.</param>
		public void PublishStatistics(string screen_name, long user_id)
		{
			if (!_authService.IsAuthorized())
			{
				throw new PostsManagerException("Текущий пользователь не авторизован.");
			}
			
			if (!int.TryParse(_configuration[POSTS_COUNT_SECTION], out int posts_count))
			{
				throw new ConfigurationException(POSTS_COUNT_SECTION);
			}

			(IEnumerable<Post>, string) posts_and_owner_name = GetPostsAndOwnerName(posts_count, screen_name);

			string statistics_message = GetStatisticsMessage(posts_and_owner_name.Item1.ToList(), posts_and_owner_name.Item2);

			_postService.Publish(new Post
			{
				OwnerId = user_id,
				OwnerType = OwnerType.User,
				Text = statistics_message
			});
			
			_logger.LogInformation($"На Вашей странице опубликован пост с сообщением: {statistics_message}");
		}

		#endregion

		#region Methods/Private

		/// <summary>
		/// Возвращает перечисление постов и имя их владельца.
		/// </summary>
		/// <param name="posts_count">Ограничение на количество постов.</param>
		/// <param name="screen_name">Короткое имя владельца.</param>
		/// <returns>Перечисление постов и имя их владельца.</returns>
		private (IEnumerable<Post>, string) GetPostsAndOwnerName(int posts_count, string screen_name)
		{
			IEnumerable<Post> posts;
			string owner_name;

			User user = _userService.GetByScreenName(screen_name);

			if (user == null)
			{
				Group group = _groupService.GetByScreenName(screen_name);

				if (group == null)
				{
					throw new PostsManagerException($"Страница с идентификатором {screen_name} не найдена.");
				}

				posts = _postService.GetByGroupId(group.Id.Value, posts_count, true);
				owner_name = group.Name;
			}
			else
			{
				posts = _postService.GetByUserId(user.Id.Value, posts_count, true);
				owner_name = user.FullName;
			}

			return (posts, owner_name);
		}

		/// <summary>
		/// Возвращает сообщение с частотностью букв в постах.
		/// </summary>
		/// <param name="posts">Посты.</param>
		/// <param name="owner_name">Имя владельца.</param>
		/// <returns>Сообщение с частотностью букв.</returns>
		private string GetStatisticsMessage(List<Post> posts, string owner_name)
		{
			if (!posts.Any())
			{
				throw new PostsManagerException($"На странице \"{owner_name}\" не найдены опубликованные её владельцем посты.");
			}

			IEnumerable<string> texts = posts.Select(p => p.Text ?? string.Empty);
			Dictionary<char, double> letter_frequency = _statisticsService.GetLetterFrequency(string.Concat(texts));

			string post_count_string;
			if (!letter_frequency.Any())
			{
				post_count_string = posts.Count == 1
					? "последнем посте"
					: $"последних {posts.Count} {(posts.Count % 10 == 1 ? "посте" : "постах")}";
				throw new PostsManagerException($"На странице \"{owner_name}\" в {post_count_string} отсутствует текст.");
			}

			// Могли получить от одного до определенного количества.
			post_count_string = posts.Count == 1
				? "последнего поста"
				: $"последних {posts.Count} {(posts.Count % 10 == 1 ? "поста" : "постов")}";

			return $"{owner_name}, статистика для {post_count_string}: {JsonConvert.SerializeObject(letter_frequency)}";
		}

		#endregion
	}
}