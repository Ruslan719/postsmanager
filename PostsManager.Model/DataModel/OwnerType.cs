﻿namespace PostsManager.Model.DataModel
{
	/// <summary>
	/// Тип владельца контента.
	/// </summary>
	public enum OwnerType
	{
		/// <summary>
		/// Пользователь.
		/// </summary>
		User,

		/// <summary>
		/// Группа.
		/// </summary>
		Group
	}
}
