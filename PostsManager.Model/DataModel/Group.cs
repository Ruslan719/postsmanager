﻿namespace PostsManager.Model.DataModel
{
	/// <summary>
	/// Группа.
	/// </summary>
	public class Group
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public long? Id { get; set; }

		/// <summary>
		/// Название.
		/// </summary>
		public string Name { get; set; }
	}
}