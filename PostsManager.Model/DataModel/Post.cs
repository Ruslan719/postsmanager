﻿namespace PostsManager.Model.DataModel
{
	/// <summary>
	/// Пост из социальной сети.
	/// </summary>
	public class Post
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public long? Id { get; set; }

		/// <summary>
		/// Идентификатор владельца, на странице которого размещается пост.
		/// </summary>
		public long OwnerId { get; set; }

		/// <summary>
		/// Тип владельца.
		/// </summary>
		public OwnerType OwnerType { get; set; }

		/// <summary>
		/// Текст.
		/// </summary>
		public string Text { get; set; }
	}
}