﻿namespace PostsManager.Model.DataModel
{
	/// <summary>
	/// Пользователь.
	/// </summary>
	public class User
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public long? Id { get; set; }

		/// <summary>
		/// ФИО.
		/// </summary>
		public string FullName { get; set; }
	}
}