﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using PostsManager.Model.Service;
using PostsManager.Model.Service.Interface;
using PostsManager.Model.Service.Vk;
using VkNet;
using VkNet.Abstractions;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace PostsManager.ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				IServiceProvider service_provider = GetServiceProvider();

				var handler = service_provider.GetService<ConsoleHandler>();
				handler.Start();
			}
			finally
			{
				LogManager.Shutdown();
			}
		}

		/// <summary>
		/// Возвращает поставщика сервисов.
		/// </summary>
		/// <returns>Поставщик сервисов.</returns>
		private static IServiceProvider GetServiceProvider() =>
			new ServiceCollection()
				.AddLogging(builder =>
				{
					builder.SetMinimumLevel(LogLevel.Trace);
					builder.AddNLog();
				})
				.AddSingleton(GetConfiguration())
				// Так как API хранит авторизационные данные и пользователь один, то сущность должна быть одна на приложение.
				// Также из-за проблем с разрешением зависимостей создание указываем явно.
				.AddSingleton<IVkApi>(sp => new VkApi())
				.AddTransient<IAuthService, VkAuthService>()
				.AddTransient<IGroupService, VkGroupService>()
				.AddTransient<IUserService, VkUserService>()
				.AddTransient<IPostService, VkPostService>()
				.AddTransient<IStatisticsService, StatisticsService>()
				.AddTransient<ConsoleHandler>()
				.AddTransient<Model.PostsManager>()
				.BuildServiceProvider();

		/// <summary>
		/// Возвращает конфигурацию приложения.
		/// </summary>
		/// <returns>Конфигурация.</returns>
		private static IConfiguration GetConfiguration() =>
			new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", false, true)
				.Build();
	}
}
