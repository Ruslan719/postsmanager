﻿using System;
using Microsoft.Extensions.Logging;
using PostsManager.Model.Exception;
using PostsManager.Model.Service.Interface;
using VkNet.Exception;

namespace PostsManager.ConsoleApp
{
	/// <summary>
	/// Обработчик консольных команд.
	/// </summary>
	public class ConsoleHandler
	{
		#region Fields

		/// <summary>
		/// Сервис авторизации.
		/// </summary>
		private readonly IAuthService _authService;

		/// <summary>
		/// Основные методы для работы с моделью.
		/// </summary>
		private readonly Model.PostsManager _postsManager;

		/// <summary>
		/// Модуль журналирования.
		/// </summary>
		private readonly ILogger<ConsoleHandler> _logger;

		#endregion

		#region Constructors

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		/// <param name="auth_service">Сервис авторизации.</param>
		/// <param name="posts_manager">Основные методы для работы с моделью.</param>
		/// <param name="logger">Модуль журналирования.</param>
		public ConsoleHandler(IAuthService auth_service, Model.PostsManager posts_manager, ILogger<ConsoleHandler> logger)
		{
			_authService = auth_service;
			_postsManager = posts_manager;
			_logger = logger;
		}

		#endregion

		#region Methods/Public

		/// <summary>
		/// Начинает обработку команд.
		/// </summary>
		public void Start()
		{
			long current_user_id = Authorize();

			string input_screen_name_text = "Введите идентификатор страницы для составления статистики: ";
			Console.Write(input_screen_name_text);
			string screen_name = Console.ReadLine();

			while (!string.IsNullOrWhiteSpace(screen_name))
			{
				try
				{
					_postsManager.PublishStatistics(screen_name, current_user_id);
				}
				catch (PostsManagerException e)
				{
					Console.WriteLine($"Ошибка! {e.Message}");
				}
				// TODO Не хотелось бы зависеть тут от используемого API. Также следует 
				catch (VkApiException e)
				{
					Console.WriteLine($"При взаимодействии с ВКонтакте произошла ошибка: {e.Message}");
				}
				catch (Exception e)
				{
					_logger.LogError(e.ToString());
					Console.WriteLine("Произошла внутренняя ошибка приложения. Подробности см. в журнале ошибок.");
				}
				finally
				{
					Console.Write(input_screen_name_text);
					screen_name = Console.ReadLine();
				}
			}

			Console.WriteLine("Завершение приложения.");
		}

		#endregion

		#region Methods/Private

		/// <summary>
		/// Авторизует пользователя.
		/// </summary>
		/// <returns>Идентификатор пользователя.</returns>
		private long Authorize()
		{
			long user_id = default(long);
			Console.WriteLine("Для работы с приложением необходимо авторизоваться.");
			while (!_authService.IsAuthorized())
			{
				try
				{
					Console.Write("Почта или телефон: ");
					string login = Console.ReadLine();
					Console.Write("Пароль: ");
					string password = ReadPassword();

					user_id = _authService.Authorize(login, password);
				}
				catch
				{
					Console.WriteLine("Не удалось авторизоваться, попробуйте ещё раз.");
					Console.WriteLine(
						"Внимание! В приложении невозможно выполнить двухфакторную авторизацию, а также пройти анти-спам проверку (ввод кода с картинки).");
				}
			}

			return user_id;
		}

		/// <summary>
		/// Считывает пароль.
		/// </summary>
		/// <returns>Пароль.</returns>
		private string ReadPassword()
		{
			string password = string.Empty;
			while (true)
			{
				ConsoleKeyInfo key = Console.ReadKey(true);
				// Backspace Should Not Work
				if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
				{
					password += key.KeyChar;
					Console.Write("*");
				}
				else
				{
					if (key.Key == ConsoleKey.Backspace && password.Length > 0)
					{
						password = password.Substring(0, (password.Length - 1));
						Console.Write("\b \b");
					}
					else if (key.Key == ConsoleKey.Enter)
					{
						Console.WriteLine();
						break;
					}
				}
			}

			return password;
		}

		#endregion
	}
}
