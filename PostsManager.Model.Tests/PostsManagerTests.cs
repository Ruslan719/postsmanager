﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using PostsManager.Model.DataModel;
using PostsManager.Model.Exception;
using PostsManager.Model.Service.Interface;

namespace PostsManager.Model.Tests
{
	/// <summary>
	/// Тесты сервиса статистики.
	/// </summary>
	public class PostsManagerTests
	{
		#region Constants

		/// <summary>
		/// Короткое имя.
		/// </summary>
		private const string SCREEN_NAME = "test_screen_name";

		/// <summary>
		/// Идентификатор группы.
		/// </summary>
		private const long GROUP_ID = -6L;

		/// <summary>
		/// Название группы.
		/// </summary>
		private const string GROUP_NAME = "test_group_name";

		/// <summary>
		/// Идентификатор пользователя.
		/// </summary>
		private const long USER_ID = -4L;

		#endregion

		#region Fields

		/// <summary>
		/// Сервис статистики.
		/// </summary>
		private PostsManager _postsManager;

		/// <summary>
		/// Сервис авторизации.
		/// </summary>
		private IAuthService _authService;

		/// <summary>
		/// Сервис групп.
		/// </summary>
		private IGroupService _groupService;

		/// <summary>
		/// Сервис пользователей.
		/// </summary>
		private IUserService _userService;

		/// <summary>
		/// Сервис постов.
		/// </summary>
		private IPostService _postService;

		/// <summary>
		/// Сервис статистики.
		/// </summary>
		private IStatisticsService _statisticsService;

		/// <summary>
		/// Интерфейс работы с конфигурацией приложения.
		/// </summary>
		private IConfiguration _configuration;

		/// <summary>
		/// Модуль журналирования.
		/// </summary>
		private ILogger<PostsManager> _logger;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и моки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_authService = Substitute.For<IAuthService>();
			_groupService = Substitute.For<IGroupService>();
			_userService = Substitute.For<IUserService>();
			_postService = Substitute.For<IPostService>();
			_statisticsService = Substitute.For<IStatisticsService>();
			_configuration = Substitute.For<IConfiguration>();
			_logger = Substitute.For<ILogger<PostsManager>>();
			_postsManager = new PostsManager(_authService, _groupService, _userService,
				_postService, _statisticsService,_configuration, _logger);

			_authService.IsAuthorized()
				.Returns(true);
			_groupService.GetByScreenName(SCREEN_NAME)
				.Returns(new Group {Id = GROUP_ID, Name = GROUP_NAME});
			int posts_count = 3;
			_configuration[PostsManager.POSTS_COUNT_SECTION].Returns(posts_count.ToString());
			_postService.GetByGroupId(GROUP_ID, posts_count, true)
				.Returns(new[] {new Post {Text = "aaa"}, new Post {Text = "b"}});
			_statisticsService.GetLetterFrequency("aaab")
				.Returns(new Dictionary<char, double> {{'a', 0.75}, {'b', 0.25}});
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что формирование статистики и её публикация отрабатывает корректно, 
		/// если передано имя существующей группы с постами.
		/// </summary>
		[Test]
		public void PublishStatistics_ExistingGroupWithPosts_PublishesPost()
		{
			_postsManager.PublishStatistics(SCREEN_NAME, USER_ID);

			_postService.Received()
				.Publish(Arg.Is<Post>(p => p.OwnerId == USER_ID && p.OwnerType == OwnerType.User &&
					p.Text.Contains(GROUP_NAME) && p.Text.Contains("0.75")));
		}

		/// <summary>
		/// Проверяет, что формирование статистики и её публикация выбрасывает исключение в случае, 
		/// когда пользователь, вызывающий метод, не авторизован.
		/// </summary>
		[Test]
		public void PublishStatistics_NotAuthorized_ThrowsException()
		{
			_authService.IsAuthorized()
				.Returns(false);

			Assert.Throws<PostsManagerException>(() => _postsManager.PublishStatistics(SCREEN_NAME, USER_ID));
		}

		/// <summary>
		/// Проверяет, что формирование статистики и её публикация выбрасывает исключение в случае, 
		/// когда по короткому имени не найден владелец страницы (пользователь или группа).
		/// </summary>
		[Test]
		public void PublishStatistics_NotExistingOwner_ThrowsException()
		{
			_groupService.GetByScreenName(SCREEN_NAME)
				.Returns((Group)null);

			Assert.Throws<PostsManagerException>(() => _postsManager.PublishStatistics(SCREEN_NAME, USER_ID));
		}

		/// <summary>
		/// Проверяет, что формирование статистики и её публикация выбрасывает исключение в случае, 
		/// когда у найденной по короткому имени группы нет постов.
		/// </summary>
		[Test]
		public void PublishStatistics_GroupHasNotPosts_ThrowsException()
		{
			_postService.GetByGroupId(GROUP_ID, Arg.Any<int>(), true)
				.Returns(new Post[0]);

			Assert.Throws<PostsManagerException>(() => _postsManager.PublishStatistics(SCREEN_NAME, USER_ID));
		}

		/// <summary>
		/// Проверяет, что формирование статистики и её публикация выбрасывает исключение в случае, 
		/// когда у найденной по короткому имени группы нет текста в постах.
		/// </summary>
		[Test]
		public void PublishStatistics_GroupHasNotTextInPosts_ThrowsException()
		{
			_statisticsService.GetLetterFrequency(Arg.Any<string>())
				.Returns(new Dictionary<char, double>(0));

			Assert.Throws<PostsManagerException>(() => _postsManager.PublishStatistics(SCREEN_NAME, USER_ID));
		}

		#endregion
	}
}