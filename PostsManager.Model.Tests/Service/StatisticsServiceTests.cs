﻿using System.Collections.Generic;
using NUnit.Framework;
using PostsManager.Model.Service;

namespace PostsManager.Model.Tests.Service
{
	/// <summary>
	/// Тесты сервиса статистики.
	/// </summary>
	public class StatisticsServiceTests
	{
		#region Fields

		/// <summary>
		/// Сервис статистики.
		/// </summary>
		private StatisticsService _statisticsService;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_statisticsService = new StatisticsService();
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что получение частотности букв в тексте с буквами возвращает корректный результат.
		/// </summary>
		[Test]
		public void GetLetterFrequency_TextWithLetters_ReturnsCorrectResult()
		{
			string text = "яяя ss Tu  !t 8 @.";

			Dictionary<char, double> result = _statisticsService.GetLetterFrequency(text);

			Assert.AreEqual(4, result.Count);
			Assert.AreEqual(3.0 / 8, result['я']);
			Assert.AreEqual(2.0 / 8, result['s']);
			Assert.AreEqual(2.0 / 8, result['t']);
			Assert.AreEqual(1.0 / 8, result['u']);
		}

		/// <summary>
		/// Проверяет, что получение частотности букв в тексте без букв возвращает пустой словарь.
		/// </summary>
		[Test]
		[TestCase("?:# 3")]
		[TestCase(null)]
		public void GetLetterFrequency_TextWithoutLetters_ReturnsEmptyResult(string text)
		{
			Dictionary<char, double> result = _statisticsService.GetLetterFrequency(text);

			Assert.IsEmpty(result);
		}

		#endregion
	}
}