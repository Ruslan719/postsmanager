﻿using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using PostsManager.Model.Service.Vk;
using VkNet.Abstractions;
using VkNet.Enums;
using VkNet.Model;
using VkNet.Utils;
using User = PostsManager.Model.DataModel.User;

namespace PostsManager.Model.Tests.Service.Vk
{
	/// <summary>
	/// Тесты сервиса пользователей.
	/// </summary>
	[TestFixture]
	public class VkUserServiceTests
	{
		#region Fields

		/// <summary>
		/// Сервис пользователей ВКонтакте.
		/// </summary>
		private VkUserService _vkUserService;

		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private IVkApi _vkApi;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и моки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_vkApi = Substitute.For<IVkApi>();
			_vkUserService = new VkUserService(_vkApi);
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что получение пользователя по короткому имени возвращает корректный результат, 
		/// если пользователь существует.
		/// </summary>
		[Test]
		public void GetByScreenName_ExistingUser_ReturnsCorrectResult()
		{
			string screen_name = "test_screen_name";
			long user_id = -5L;
			string first_name = "test_first_name", last_name = "test_last_name";

			_vkApi.Utils.ResolveScreenName(screen_name)
				.Returns(new VkObject
				{
					Id = user_id,
					Type = VkObjectType.User
				});

			_vkApi.Users.Get(Arg.Is<IEnumerable<long>>(ids => ids.Contains(user_id)))
				.Returns(new[]
				{
					new VkNet.Model.User
					{
						Id = user_id,
						FirstName = first_name,
						LastName = last_name
					}
				}.ToReadOnlyCollection());

			User result = _vkUserService.GetByScreenName(screen_name);

			Assert.AreEqual(user_id, result.Id);
			Assert.AreEqual($"{first_name} {last_name}", result.FullName);
		}

		/// <summary>
		/// Проверяет, что получение пользователя по короткому имени возвращает null, 
		/// если пользователь с указанным именем не существует.
		/// </summary>
		[Test]
		public void GetByScreenName_NotExistingUser_ReturnsNull()
		{
			string screen_name = "test_screen_name";

			_vkApi.Utils.ResolveScreenName(screen_name)
				.Returns((VkObject)null);

			User result = _vkUserService.GetByScreenName(screen_name);

			Assert.IsNull(result);
		}

		#endregion
	}
}