﻿using NSubstitute;
using NUnit.Framework;
using PostsManager.Model.Service.Vk;
using VkNet.Abstractions;
using VkNet.Enums;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Utils;
using Group = PostsManager.Model.DataModel.Group;

namespace PostsManager.Model.Tests.Service.Vk
{
	/// <summary>
	/// Тесты сервиса групп.
	/// </summary>
	[TestFixture]
	public class VkGroupServiceTests
	{
		#region Fields

		/// <summary>
		/// Сервис групп ВКонтакте.
		/// </summary>
		private VkGroupService _vkGroupService;

		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private IVkApi _vkApi;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и моки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_vkApi = Substitute.For<IVkApi>();
			_vkGroupService = new VkGroupService(_vkApi);
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что получение группы по короткому имени возвращает корректный результат, 
		/// если группа с указанным именем существует.
		/// </summary>
		[Test]
		public void GetByScreenName_ExistingGroup_ReturnsCorrectResult()
		{
			string screen_name = "test_screen_name";
			long group_id = -3L;
			string group_name = "test_group_name";

			_vkApi.Utils.ResolveScreenName(screen_name)
				.Returns(new VkObject
				{
					Id = group_id,
					Type = VkObjectType.Group
				});

			_vkApi.Groups.GetById(null, screen_name, Arg.Any<GroupsFields>())
				.Returns(new[]
				{
					new VkNet.Model.Group
					{
						Id = group_id,
						Name = group_name
					}
				}.ToReadOnlyCollection());

			Group result = _vkGroupService.GetByScreenName(screen_name);

			Assert.AreEqual(group_id, result.Id);
			Assert.AreEqual(group_name, result.Name);
		}

		/// <summary>
		/// Проверяет, что получение группы по короткому имени возвращает null, 
		/// если группа с указанным именем не существует.
		/// </summary>
		[Test]
		public void GetByScreenName_NotExistingGroup_ReturnsNull()
		{
			string screen_name = "test_screen_name";

			_vkApi.Utils.ResolveScreenName(screen_name)
				.Returns((VkObject)null);

			Group result = _vkGroupService.GetByScreenName(screen_name);

			Assert.IsNull(result);
		}

		#endregion
	}
}