﻿using Microsoft.Extensions.Configuration;
using NSubstitute;
using NUnit.Framework;
using PostsManager.Model.Service.Vk;
using VkNet.Abstractions;
using VkNet.Model;

namespace PostsManager.Model.Tests.Service.Vk
{
	/// <summary>
	/// Тесты сервиса авторизации.
	/// </summary>
	[TestFixture]
	public class VkAuthServiceTests
	{
		#region Fields

		/// <summary>
		/// Сервис авторизации ВКонтакте.
		/// </summary>
		private VkAuthService _vkAuthService;

		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private IVkApi _vkApi;

		/// <summary>
		/// Интерфейс работы с конфигурацией приложения.
		/// </summary>
		private IConfiguration _configuration;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и моки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_vkApi = Substitute.For<IVkApi>();
			_configuration = Substitute.For<IConfiguration>();
			_vkAuthService = new VkAuthService(_vkApi, _configuration);
			
			_configuration[VkAuthService.VK_APPLICATION_ID_SECTION].Returns("1");
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что авторизация пользователя вызывает метод API с корректным параметром.
		/// </summary>
		[Test]
		public void Authorize_SomeParams_CallsApiMethod()
		{
			long user_id = -14L;

			string login = "test_login", password = "test_pass";

			_vkApi.UserId.Returns(user_id);

			long result = _vkAuthService.Authorize(login, password);

			_vkApi.Received()
				.Authorize(Arg.Is<ApiAuthParams>(p =>
					p.Login == login && p.Password == password && p.Settings != null && p.ApplicationId > 0));

			Assert.AreEqual(user_id, result);
		}

		/// <summary>
		/// Тестирует, что проверка	авторизации пользователя возвращает false, если авторизация не проходила.
		/// </summary>
		[Test]
		public void IsAuthorized_NotAuthorized_ReturnsFalse()
		{
			bool result = _vkAuthService.IsAuthorized();
			
			Assert.IsFalse(result);
		}

		#endregion
	}
}