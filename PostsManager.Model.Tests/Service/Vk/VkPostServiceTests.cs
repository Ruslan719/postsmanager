﻿using System;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using PostsManager.Model.DataModel;
using PostsManager.Model.Service.Vk;
using VkNet.Abstractions;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using Post = PostsManager.Model.DataModel.Post;

namespace PostsManager.Model.Tests.Service.Vk
{
	/// <summary>
	/// Тесты сервиса постов.
	/// </summary>
	[TestFixture]
	public class VkPostServiceTests
	{
		#region Fields

		/// <summary>
		/// Сервис постов ВКонтакте.
		/// </summary>
		private VkPostService _vkPostService;

		/// <summary>
		/// API для работы с ВКонтакте.
		/// </summary>
		private IVkApi _vkApi;

		#endregion

		#region SetUp

		/// <summary>
		/// Инициализирует поля и моки.
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			_vkApi = Substitute.For<IVkApi>();
			_vkPostService = new VkPostService(_vkApi);
		}

		#endregion

		#region Tests

		/// <summary>
		/// Проверяет, что получение постов со страницы группы возвращает корректный результат.
		/// </summary>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные группой.</param>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void GetByGroupId_ExistingGroup_ReturnsCorrectResult(bool only_from_owner)
		{
			long group_id = 6L, post_id = 2L;
			int count = 1;
			string post_text = "test_text";
			var wall_filter = only_from_owner ? WallFilter.Owner : WallFilter.All;

			_vkApi.Wall.Get(Arg.Is<WallGetParams>(p =>
					p.OwnerId == -Math.Abs(group_id) && p.Count == (ulong)count && p.Filter == wall_filter))
				.Returns(new WallGetObject
				{
					WallPosts = new[]
					{
						new VkNet.Model.Post
						{
							Id = post_id,
							OwnerId = -Math.Abs(group_id),
							Text = post_text
						}
					}.ToReadOnlyCollection()
				});

			Post result = _vkPostService.GetByGroupId(group_id, count, only_from_owner)
				.Single();

			Assert.AreEqual(Math.Abs(group_id), result.OwnerId);
			Assert.AreEqual(post_id, result.Id);
			Assert.AreEqual(post_text, result.Text);
			Assert.AreEqual(OwnerType.Group, result.OwnerType);
		}

		/// <summary>
		/// Проверяет, что получение постов со страницы пользователя возвращает корректный результат.
		/// </summary>
		/// <param name="only_from_owner">Вернуть только посты, опубликованные пользователем.</param>
		[Test]
		[TestCase(true)]
		[TestCase(false)]
		public void GetByUserId_ExistingUser_ReturnsCorrectResult(bool only_from_owner)
		{
			long user_id = -1L, post_id = 9L;
			int count = 1;
			string post_text = "test_text";
			var wall_filter = only_from_owner ? WallFilter.Owner : WallFilter.All;

			_vkApi.Wall.Get(Arg.Is<WallGetParams>(p =>
					p.OwnerId == Math.Abs(user_id) && p.Count == (ulong)count && p.Filter == wall_filter))
				.Returns(new WallGetObject
				{
					WallPosts = new[]
					{
						new VkNet.Model.Post
						{
							Id = post_id,
							OwnerId = Math.Abs(user_id),
							Text = post_text
						}
					}.ToReadOnlyCollection()
				});

			Post result = _vkPostService.GetByUserId(user_id, count, only_from_owner)
				.Single();

			Assert.AreEqual(Math.Abs(user_id), result.OwnerId);
			Assert.AreEqual(post_id, result.Id);
			Assert.AreEqual(post_text, result.Text);
			Assert.AreEqual(OwnerType.User, result.OwnerType);
		}

		/// <summary>
		/// Проверяет, что публикация поста вызывает метод API с корректным параметром.
		/// </summary>
		[Test]
		public void Publish_SomePost_CallsApiMethod()
		{
			var post = new Post
			{
				OwnerType = OwnerType.Group,
				OwnerId = 42L,
				Text = "test_text"
			};

			_vkPostService.Publish(post);

			_vkApi.Received()
				.Wall.Post(new WallPostParams {OwnerId = -post.OwnerId, Message = post.Text});
		}

		#endregion
	}
}